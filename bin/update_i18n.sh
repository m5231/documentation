#!/usr/bin/env bash

# You need to run this script every time you modified rst files.
# This will update po files messages and may be break translation.
# Then edit modified po files to repair translation...

# Generate pot files from rst files in build/locale
make gettext

# Generate po files from pot files in source/locale
# (use french (France) with label fr_FR)
sphinx-intl update -p build/gettext -d source/locale/ -l fr_FR
