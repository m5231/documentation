#!/usr/bin/env bash

# set build release version in ENV to propagate it in documents
export BUILD_RELEASE=`cat ./RELEASE`

# set MADAM version in ENV to propagate it in documents
export MADAM_VERSION=`cat ./VERSION`

# public dir path
PUBLICDIR=build/public

# if public dir not exists...
if [ ! -d $PUBLICDIR ]; then
  # create public dir
  mkdir -p $PUBLICDIR
fi

# if en_US old dir exists...
if [ -d $PUBLICDIR/en_US ]; then
  # remove it
  rm -rf $PUBLICDIR/en_US
fi

# clear doctrees folder
if [ -d $PUBLICDIR/doctrees ]; then
    rm -r $PUBLICDIR/doctrees
fi

# HTML
# use tag for only directives (adapt image sizes for output)
make html SPHINXOPTS="-t html -D release='${BUILD_RELEASE}'" BUILDDIR=$PUBLICDIR

# rename output dir to en_US
mv $PUBLICDIR/html $PUBLICDIR/en_US
