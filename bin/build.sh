#!/usr/bin/env bash

# set build release version in ENV to propagate it in documents
export BUILD_RELEASE=`cat ./RELEASE`

# set MADAM version in ENV to propagate it in documents
export MADAM_VERSION=`cat ./VERSION`

# build english version in public/en_US folder
./bin/build_en_US.sh

# build french version in public/fr_FR folder
./bin/build_fr_FR.sh

# add index in public folder
echo "create main index of documentations..."
cp -r templates/* build/public/.
