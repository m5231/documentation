Installation
============

MADAM cluster looks like this:

.. uml:: ./_static/madam_suite.plantuml

It requires a Docker Swarm network of nodes.

Docker Swarm
############

.. note::

    MADAM run on a Docker Swarm cluster. This documentation does not cover the subject, so you should have some knowledge of it before installing MADAM.

See `the Docker Swarm Mode documentation <https://docs.docker.com/engine/swarm/>`_.

Configuration
#############

MADAM use a configuration file in Yaml format.

A ``madam_deploy.yaml.example`` file is provided in `MADAM repository <https://gitlab.com/m5231/madam>`_ for the Docker Swarm service deployment. You should modify it to suit your needs.

The file contains three parts:

Server
------

This part specify the **host** and **port** for the GraphQL API.
MADAM will listen GraphQL requests on the domain/IP and port specified.

.. code-block:: yaml

    server:
      host: 0.0.0.0
      port: 5000

Database
________

This part specify the **host** and **port** and other parameters of the PostgreSQL server.

Database **migrations** requires more permissions on the database so has a separate **username** to login.

Connection **retries** only apply when MADAM server is started.

.. code-block:: yaml

    database:
      host: postgresql # name use in docker-compose
      port: 5432
      username: madam
      password: xxxx
      name: madam
      ssl_mode: disable  # require (=yes), disable (=no), allow (=try no ssl, retry ssl if failed)
      migrations:
        username: postgres
        password: xxxx
      retries: 3
      retries_idle: 30

Docker
______

This part specify parameters needed by Docker.

**base_url**: is the docker daemon used by MADAM to run agent as swarm services.
You can use the Docker api if you have enabled it.

**networks**: list the networks MADAM agents swarm services can see.

**mounts**: list the volumes to mount in MADAM container and agent containers as well. It should be shared volumes.

**applications**: list of application **names** used by agents. Each **name** contains a **version** key ("latest" by default), each **version** key has a **value** which is the name of the docker image to use.

In the exemple below, we have added a python version with the key **my_version_37**. Then, in the Zeebe workflow, we will set the header **applications.python.version** to **my_version_37** to tell a workflow node to use this version.

.. code-block:: yaml

    docker:
      base_url: "unix://var/run/docker.sock"
      networks: []
      mounts:
        - /tmp/madam_tests:/tmp/madam_tests
        # font path for ffmpeg drawtext
        - /usr/share/fonts:/usr/share/fonts
      applications:
        ffmpeg:
          latest: "jrottenberg/ffmpeg"
        melt:
          latest: "mltframework/melt"
        bash:
          latest: "bash"
        python:
          latest: "python:3.8-slim-buster"
          my_version_37: "python:3.7-slim-buster"
        imagemagick:
          latest: "dpokidov/imagemagick"
        mediainfo:
          latest: "jlesage/mediainfo"


Manager node
############

On the manager node, you should pull images and start **swarm services** for:

* **MADAM** from `the official MADAM docker image <https://hub.docker.com/r/vitexier/madam>`_.
* **PostgreSQL** from `the official PostgreSQL docker image <https://hub.docker.com/_/postgres>`_.

Environment variables
---------------------

MADAM swarm service requires two environment variables to run properly:

**MADAM_LOG_LEVEL**: set the verbosity level of the logs.
**MADAM_CONFIG_PATH**: the path of the yaml configuration file.

It also requires to publish the **port** number set in the **server** part of the configuration file.

Here is an exemple, extracted from the MADAM repository docker-compose.yml for a local deployment.

.. note::

    MADAM config file contains passwords, so we use the swarm secrets here. It is recommended to do it.

.. code-block:: yaml

    services:
      madam:
        image: "madam:0.5.5"
        environment:
          MADAM_LOG_LEVEL: "DEBUG"
          MADAM_CONFIG_PATH: "/run/secrets/madam.yaml"
        ports:
          - "5000:5000"
        volumes:
            - "/tmp/madam_tests:/tmp/madam_tests"
            - /var/run/docker.sock:/var/run/docker.sock
        networks:
          - madam_network
        secrets:
          - madam.yaml

Worker nodes
############

On all the worker nodes, you should pull images of all the agent applications:

* `FFmpeg docker image. <https://hub.docker.com/r/jrottenberg/ffmpeg>`_
* `Melt docker image. <https://hub.docker.com/r/mltframework/melt>`_
* `ImageMagick docker image. <https://hub.docker.com/r/dpokidov/imagemagick>`_
* `MediaInfo docker image. <https://hub.docker.com/r/jlesage/mediainfo>`_
* `Python docker image. <https://hub.docker.com/r/python:3.10-slim-buster>`_
* `Bash docker image. <https://hub.docker.com/r/bash>`_

Client station
##############

Apart from the cluster, a desktop application is required to create Zeebe BPMN workflows:

* `Camunda Modeler 4.11+ <https://camunda.com/download/modeler/>`_

If everything went fine, you should be able to access the GraphQL playground by using the url of MADAM GraphQL API:

.. code-block::

    http://manager_node_ip_or_domain:5000

You can send your BPMN workflows files to your server with the minimalist madam cli commands by installing the `madam-mam python package <https://pypi.org/project/madam-mam>`_ from PyPI repository.
See the :doc:`next chapter </client>`.
