Distributed workflow
====================

MADAM can split video in chunk to distributed the process on one video to multiple nodes.

In this chapter, we will learn about BPMN parallel multi-instances and subprocesses.

.. image:: _static/distributed_scene_chunked_workflow.png

Please `download this example file <_static/bpmn/ffmpeg_chunked.bpmn>`_ to follow this guide.

This workflow example create three video files in parallel (thanks to parallel multi-instance of a task).

Then, a subprocess is executed on each file, in parallel (thanks to parallel multi-instance of a subprocess).

In this subprocess, we detect the scenes, then for each scene, in parallel, we extract a chunk from the file and encode it as needed.

As AAC audio codec does not support split/concat, we extract the audio track and encode it as a whole, in parallel of the video processing.

Finally, we concatenate all the video chunks and mux the resulting track with the audio track in a video container.

Multi-instance task
###################

The first task with ID ``ffmpeg_create`` is a multi instance task.
To create one, you should use the tool symbol then click on the three vertical bars (parallel).

.. image:: _static/distributed_multi_instance_selector.png

It adds a section **Multi-instance** in the properties panel.

Here you can set the list of items to process in **Input collection** and a variable name for each item in **Input element**.

As you can see in the example file, **Input collection** is evaluated as a python expression if it start with "=".
Each value of the list given here populate a variable named after the **Input element**.

The task is triggered as many times as the list length. All the instances start in parallel as we have chosen this option.
But we can choose a sequential execution by choosing the three horizontal bars symbol.

=====================   ======================================================================================================
Multi-instance          Values
=====================   ======================================================================================================
**Input collection**    =["/tmp/madam_tests/video_01.mkv", "/tmp/madam_tests/video_02.mkv", "/tmp/madam_tests/video_03.mkv"]
**Input element**       = destination
=====================   ======================================================================================================

Ok so, this task create three videos. But how can we tell the next node which element to process?

To do that, you have to set the **Output collection** and **Output element** fields.
Set the name of the variable containing the list of the items to process next in **Output collection**, let's say *items*.
Then set the name of the variable from which the content is added to the list, in **Output element**. In this case,
*destination* is the variable containing the file path to process.

=====================   ====================
Multi-instance          Values
=====================   ====================
**Output collection**   items
**Output element**      = destination
=====================   ====================

Now, each time the task end an instance, it append the content of *destination* to the dictionary named *items*.

.. warning::

    **Output collection** is always a *Dictionary* (as in the Python language).
    It's important to know that if you want to use complex python expression with this variable.

Multi-instance subprocess
#########################

We have three files, and we want to do a complex process on them. So we create an Expanded Subprocess node after the first task.
But we also want to do the three subprocess in parallel. As for the first task, we select the three vertical bars to do it in parallel.

.. image:: _static/distributed_multi_instance_subprocess.png

The **Multi-instance** section of the properties panel is quite understandable now.
It takes the *items* dictionary given by the previous task, and put each entry in the *source* variable.

At the end of the subprocess workflow, the subprocess put the content of the *destination* variable in the *destinations* dictionary.
It is useless here, as there is no more task after that, but this information can be useful for a remote client.

=====================   ======================================================================================================
Multi-instance          Values
=====================   ======================================================================================================
**Input collection**    = items
**Input element**       source
**Output collection**   destinations
**Output element**      = destination
=====================   ======================================================================================================

Get scenes
##########

.. image:: _static/distributed_get_scenes.png

Nothing to do here as the subprocess gives the *source* variable required by the agent.
The header field **diff** is set here for the example, but is optional. You can change it to adjust the detection sensibility.

The ``scenes`` agent outputs detected scenes as a list of dictionaries in the *scenes* output variable.

.. code-block:: python

                    {
                        "index": int,  # int with scene index number
                        "start": str,  # Start timecode in second format like 00:00:00:00.000
                        "end": str,  # End timecode in second format like 00:00:00:00.000
                        "duration": str,  # Duration timecode in second format like 00:00:00:00.000
                    }

This output will be useful for FFmpeg in the next task.

Split and encode
################

.. image:: _static/distributed_split_encode.png

This is the heart of the workflow. Here we extract scenes from the video source as defined by the *scenes* variable.
It is a multi-instance parallel task.

The FFmpeg command is executed once for each item in *items* variable. We collect each item in the *source* variable.
This variable is used in the agent **arguments** header.

=====================   ======================================================================================================
Multi-instance          Values
=====================   ======================================================================================================
**Input collection**    = scenes
**Input element**       scene
**Output collection**   scene_files
**Output element**      = scene_file
=====================   ======================================================================================================

After each FFmpeg execution, the *scene_file* content is added to the *scene_files* dictionary, for the next task.

In the **Input** section, we set the filepath of the extracted video file in the *destination* variable.

=============================   ========================================================================
Input                           Values
=============================   ========================================================================
**Local Variable Name**         destination
**Variable Assignment Value**   = f"/tmp/madam_tests/{Path(source).stem}_scene_{scene['index']:04d}.mp4"
=============================   ========================================================================

We set also the **Output** section to populate the global variable *scene_file*, with the same filepath:

=============================   ========================================================================
Output                           Values
=============================   ========================================================================
**Process Variable Name**       scene_file
**Variable Assignment Value**   = f"/tmp/madam_tests/{Path(source).stem}_scene_{scene['index']:04d}.mp4"
=============================   ========================================================================

This is the magic line that extract the *scene* from the *source* filepath and encode it to the desired format in the *destination* filepath:

==============  ==========================================================================================================
Header          Values
==============  ==========================================================================================================
**arguments**   -y -i {{source}} -ss {{scene['start']}} -t {{scene['duration']}} -f mp4 -map 0:0 -c:v h264 {{destination}}
==============  ==========================================================================================================

Concatenate
###########

.. image:: _static/distributed_concatenate.png

This task get all the video scenes and concatenate them in a full video file. The agent requires two variable names,
so we create them in the **Input** section with the appropriate values.

=============================   ========================================================================
Input                           Values
=============================   ========================================================================
**Local Variable Name**         destination
**Variable Assignment Value**   = f"/tmp/madam_tests/{Path(source).stem}_video.mp4"
**Local Variable Name**         sources
**Variable Assignment Value**   = sorted(scene_files.values())
=============================   ========================================================================

We also create a new global variable *video_destination* with the resulting video filepath.

=============================   ========================================================================
Output                           Values
=============================   ========================================================================
**Process Variable Name**       video_destination
**Variable Assignment Value**   = f"/tmp/madam_tests/{Path(source).stem}_video.mp4"
=============================   ========================================================================

Audio encode
############

.. image:: _static/distributed_audio_encode.png

The audio encoding is done in parallel of the video tasks.

We set the local variable *destination* used to specify the resulting audio filepath.

=============================   ========================================================================
Input                           Values
=============================   ========================================================================
**Local Variable Name**         destination
**Variable Assignment Value**   = f"/tmp/madam_tests/{Path(source).stem}_audio.mkv"
=============================   ========================================================================

We need the same information in the global variables, so we set it in *audio_destination*:

=============================   ========================================================================
Output                           Values
=============================   ========================================================================
**Process Variable Name**       audio_destination
**Variable Assignment Value**   = f"/tmp/madam_tests/{Path(source).stem}_audio.mkv"
=============================   ========================================================================

This is the audio FFmpeg command *arguments* from the **Header** section:

==============  ==========================================================================================================
Header          Values
==============  ==========================================================================================================
**arguments**   -y -i {{source}} -f matroska -map 0:1 -c:a aac {{destination}}
==============  ==========================================================================================================

Multiplexer
###########

.. image:: _static/distributed_multiplexer.png

We need to mux together the video and audio files. We use FFmpeg again to do it.

We create the resulting filepath in the local variable *destination*.

=============================   ========================================================================
Input                           Values
=============================   ========================================================================
**Local Variable Name**         destination
**Variable Assignment Value**   = f"/tmp/madam_tests/{Path(source).stem}_encoded.mp4"
=============================   ========================================================================

We need the same information in the global variables:

=============================   ========================================================================
Output                           Values
=============================   ========================================================================
**Process Variable Name**       destination
**Variable Assignment Value**   = f"/tmp/madam_tests/{Path(source).stem}_encoded.mp4"
=============================   ========================================================================

We use the global variables *video_destination* and *audio_destination* as sources and the local variable *destination* as the resulting filepath.

==============  ==========================================================================================================
Header          Values
==============  ==========================================================================================================
**arguments**   -y -i {{video_destination}} -i {{audio_destination}} -map 0:0 -map 1:0 -f mp4 {{destination}}
==============  ==========================================================================================================

End events
##########

We have an End event for the subprocess and an End event for the main process. Look at the *properties panel*,
there is an **Output** section to map global variables to other names for the database, but it is not necessary here.

Fixed size chunks
#################

To split a video by scenes is a good thing when you are dealing with a final product. But if you are dealing with rushes or dailies,
the chunks will be too big. To overcome this, you should use fixed size chunks.

`Download the example file <_static/bpmn/ffmpeg_fixed_chunks.bpmn>`_ to see how to use fixed size chunks.

That's it. Now you know how to split and encode video chunks in parallel with multi-instance tasks and subprocesses.
