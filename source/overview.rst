Overview
========

**MADAM** is the acronym of Multi Agent Distributed Asset Manager.

MADAM is a bunch of open source and free (as freedom) software to operate on a big amount of media files.

* Scalability: use your cluster of machines that can extend easily.
* On premise: MADAM can be installed locally.
* In the cloud: MADAM could be hosted in the cloud (docker container based).
* Highly distributed.
* Easy visual configuration of workflows with `BPMN standard <https://www.bpmn.org/>`_.
* Really multi-media (can handle virtually all file formats).

Is MADAM a MAM?
###############

Yes and... No! MADAM is not a traditional MAM as it is not a Hardware/Software monolithic solution.
It rely on your own shared storage solution and cluster of computers to execute workflows.

* MADAM use the BPMN standard to define workflows offering a visual and efficient way to create them.
* Tasks are distributed on a cluster of servers.
* It includes a Watchfolder system to automate workflow triggering.

So the exact response is: MADAM is the "Big Data" way to handle Media Asset Management, not the classic monolithic MAM way.
Its core business logic is to manage workflows.

Big data
########

This term is only used here to specify that the architecture is distributed.
This allows to split FFmpeg or Melt jobs by splitting videos in chunks and process them in parallel on many computers.

More specifically, MADAM users creates BPMN workflow XML files using the `Camunda Modeler 4.11+ <https://camunda.com/download/modeler/>`_.
Then they send the file to MADAM GraphQL API to execute the workflow on a bunch of data stored on a shared storage.
Python workers execute tasks on available nodes using containers. This architecture allow MADAM to run on the cloud (not tested).

MADAM relies on a PostgreSQL database server to store logs and other informations.

TODO
####

* Add media indexers as agent to create media library indices.
* Add metadata to identify files in indices and easily search and retrieve the files.
* Web interface.
