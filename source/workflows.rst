Create a workflow file
======================

This is a short tutorial to get the basics of tasks in the Camunda Modeler.

Install Camunda modeler
#######################

Download `the last version of Camunda modeler <https://camunda.com/download/modeler/>`_.

You can `download this example <_static/bpmn/ffmpeg.bpmn>`_ before create your own with the following procedure.

Workflow ID
###########

Create a new empty workflow. A tab with a section named **General** allow you to configure the name **Name** and the unique **ID** of the workflow.

.. image:: _static/camunda_modeler_workflow_id.png

.. warning::

    The unique ID of the workflow identify each workflows in your the database.
    It should not have spaces, and only letters and digits, dashes or underscores.
    All files with the same **ID** sent to the server will be added to the same workflow as a new version,
    if not already sent.

Start Event
###########

All workflows should start with a **Start Event**. Create one and look at the *properties panel* of this node.

.. image:: _static/workflow_start_event.png

A default **name** and **ID** is proposed for the node, in the **General** section.
I strongly recommend that you use an explicit **name** and **ID** for this special node.

So let's use the **Name** *Start* and the **ID** *start*.

=========   =========
General     Values
=========   =========
**Name**    Start
**ID**      start
=========   =========

Here you can initialize some *global variables*, in the **Output** section of the node, for your workflow.
It can be the main path of the data, or anything else. Let's leave it empty for this tutorial.

When clicked on the node, some icons are proposed. Click on the rectangular shape to create a **Task**.

Service Task
############

Click on the tool icon to select the type of the task. Choose **Service Task**.

.. image:: _static/workflow_service_task.png

The *properties panel* allow us to configure the service.

.. image:: _static/workflow_service_params.png

In the **General** section, let's give it an explicit **Name** and **ID**.

=========   ====================
General     Values
=========   ====================
**Name**    FFmpeg Create Video
**ID**      ffmpeg_create_video
=========   ====================

In the **Task Definition** section, let's select an agent to do the task.
In the field **Type**, set the name of the agent, here we choose the **ffmpeg** agent.

=============== =========================
Task Definition Values
=============== =========================
**Type**        ffmpeg
**Retries**     *Leave blank (default=3)*
=============== =========================

Input Variables
---------------

Let's see the **Input** section now. It allows you to map *global variable* values to *local variable* names.
All agents will have access only to the input *local variables*, populated with all the *global variables*.
But it will often happen that a *local variable* name required by the agent does not exists in the *global variables*.
Then you will have to map the desired *global variable* value in the required *local variable* name.

.. image:: _static/workflow_service_inputs.png

You should have a **+ sign** to click on and create an input *local variable*.

Give it *destination* as the **Local Variable Name**.

=============================   =====================================================
Input                           Values
=============================   =====================================================
**Local Variable Name**         destination
**Variable Assignment Value**   = "/your/shared/storage/path/ffmpeg_video_create.mp4"
=============================   =====================================================

For the **Variable Assignment Value**, you can give it a *fixed value* or an *Python expression*.
MADAM will evaluate it as a *Python expression* (why the Python langage? eh, MADAM is written in Python after all ;) ).

.. note::

    It is a good habit to always set the "= sign" at the beginning of the field,
    to tell MADAM that this is a *Python expression*, not a string.
    Let's say you have a *global variable* in your workflow named *source_filename*,
    then you can use a *Python expression* like ``= f"/path/to/your/shared/storage/{source_filename}"``.

So give this variable value a path and a filename like:

.. code-block:: python

    = "/your/shared/storage/path/ffmpeg_video_create.mp4"

Output Variables
----------------

All *local output variables* created by the agent, if any, are published as *global variables*.
The **Output** section allow you to map *local output variable* value created by the agent to another *global variable* name.

We do not need it for this tutorial.

Headers
-------

A **Header** allow you to configure the agent behaviour. You create a header with the + sign, giving it a **Key** and a **Value**.
The **Key** is the name of the configuration **Key** to set up or modify,
and the **Value** is the configuration **Value** that you want for this **Key**.

*ffmpeg* agent use the latest version of FFmpeg. But an optional header allow us to use another version
(the version should be declared in ``madam.yaml`` configuration file).

So, just for the exercise, let's tell the agent to use the *latest* version of FFmpeg.

.. image:: _static/workflow_service_headers.png

Create a new **Header** with the **Key** *applications.ffmpeg.version* and the **Value** *latest*.

Then, most importantly, we need to configure the arguments of the FFmpeg application that will be run by the agent.

Create a new **Header** with the **Key** *arguments* and the following **Value**:

.. code-block::

    -y -f lavfi -i smptebars=duration=5:size=qcif:rate=25 {{destination}}

================================    ======================================================================
Header                              Values
================================    ======================================================================
**arguments**                       -y -f lavfi -i smptebars=duration=5:size=qcif:rate=25 {{destination}}
**applications.ffmpeg.version**     latest
================================    ======================================================================

This configuration tells FFmpeg to create a 5 seconds video with smpte bars.
The video filepath is our *destination* variable given here as a Jinja2 tag.

Yes, application arguments headers are evaluated as Jinja2 templates!
This allow you to dynamically control what an agent will do, depending of the variable values of the previous tasks.

See `the Jinja2 documentation <https://jinja2docs.readthedocs.io/en/stable/>`_ to know more about it.

.. warning::

    Remember that an agent only see the *input local variables*, which are a copy of the *global variables*,
    initialized before the agent execution, and modified by the **Input** tab mapping.

See the full configuration fields for all agents :doc:`in the dedicated chapter </agents>`.

End Event
#########

All workflow should terminate with an **End Event**. Click on the thick circle to create one.

.. image:: _static/workflow_end_event.png

Lets name it explicitly with the **Name** *End* and the **ID** *end*.

=========   =========
General     Values
=========   =========
**Name**    End
**ID**      end
=========   =========

Save your masterpiece with the **.bpmn** extension.

Go to the :doc:`/client` chapter to see how to execute it.

To create distributed workflows with video chunks, :doc:`go to this chapter </distributed>`.
