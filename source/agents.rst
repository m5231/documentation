.. MADAM documentation master file, created by
   sphinx-quickstart on Tue Apr  7 16:31:46 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Agents
======

All agents configuration in Camunda Modeler are detailed here. To use an agent in your workflow, first create a *Service Task*,
then configure the tab sections of the task following this reference page.

The **Task Definition** row specify the *Type* to set in Camunda modeler *Task Definition* section to use this agent.

The **Input** rows list the mandatory variable names expected by the agent in input.
You can use the *Input* section in the Camunda modeler to map global variable names to those names.

The **Output** rows list the variable names created by the agent as its output.
You can use the *Output* section in the Camunda modeler to map those names to other global variable names.

The **Header** rows list the configuration to set in the *Header* section of the Camunda modeler.

The **Default** column gives the default value for optional fields.

Bash
####

Execute application bash with arguments provided.

**Headers:**
   **arguments:** string with bash command or script path, parsed as a jinja2 string
                  (workflow variables can be used with jinja2 tags, see https://jinja.palletsprojects.com/en/3.0.x/templates/#variables)

   **applications.bash.version:** Optional[string] with version for bash label application

Examples:
   arguments = "cp {{source}} {{destination}}"

   arguments = "{{script_path}}"

===================  ===========================   ================= =========
Section              Key                           Value             Default
===================  ===========================   ================= =========
**Task Definition**  Type                          bash
**Input**
**Output**
**Header**           arguments                     *jinja2 template*
\                    applications.bash.version     *str*             latest
===================  ===========================   ================= =========

Concatenate
###########

Concatenate a list of media sources into a destination file (with FFmpeg).

**Input Variables:**
   **sources:** list of string of the source paths

   **destination:** string path of the destination file

**Headers:**
   **applications.ffmpeg.version:** Optional[string] with version for ffmpeg label application

.. note::

   This agent will create and remove a temporary playlist file from sources list in the destination directory

===================  ================================ ================= =========
Section              Key                              Value             Default
===================  ================================ ================= =========
**Task Definition**  Type                             concatenate
**Input**            sources                          *List[str]*
\                    destination                      *str*
**Output**
**Header**           applications.concatenate.version *str*             latest
===================  ================================ ================= =========

FFmpeg
######

Execute application FFmpeg with arguments provided.

**Headers:**

   **arguments:** string with FFmpeg arguments parsed as a jinja2 string
                  (workflow variables can be used with jinja2 tags, see https://jinja.palletsprojects.com/en/3.0.x/templates/#variables)

   **applications.ffmpeg.version:** Optional[string] with version for ffmpeg label application

Example:
   arguments = "-y -i {{item}} {{destination}}"

===================  ===========================   ================= =========
Section              Key                           Value             Default
===================  ===========================   ================= =========
**Task Definition**  Type                          ffmpeg
**Input**
**Output**
**Header**           arguments                     *jinja2 template*
\                    applications.ffmpeg.version   *str*             latest
===================  ===========================   ================= =========

ImageMagick
###########

Execute ImageMagick commands with arguments provided.

**Headers:**
   **command:** string with ImageMagick command to use
               Supported commands are:
               - magick
               - magick-script
               - compare
               - composite
               - conjure
               - convert
               - identify
               - mogrify
               - montage

   **arguments:** string with ImageMagick arguments parsed as a jinja2 string
                  (workflow variables can be used with jinja2 tags, see https://jinja.palletsprojects.com/en/3.0.x/templates/#variables)

   **applications.imagemagick.version:** Optional[string] with version for imagemagick label application

Example:
   arguments = "-size 100x100 {{source}} {{destination}}"

===================  ================================ ================= =========
Section              Key                              Value             Default
===================  ================================ ================= =========
**Task Definition**  Type                             imagemagick
**Input**
**Output**
**Header**           command                          - magick
                                                      - magick-script
                                                      - compare
                                                      - composite
                                                      - conjure
                                                      - convert
                                                      - identify
                                                      - mogrify
                                                      - montage
\                    arguments                        *jinja2 template*
\                    applications.imagemagick.version *str*             latest
===================  ================================ ================= =========

Mediainfo
#########

Execute application Mediainfo with arguments provided.

**Output Variables:**
   **mediainfo:** dict with mediainfo output

**Headers:**
   **arguments:** string with Mediainfo arguments parsed as a jinja2 string
                  (workflow variables can be used with jinja2 tags, see https://jinja.palletsprojects.com/en/3.0.x/templates/#variables)
                  --Output=? is useless as it will be overloaded by the agent to --Output=JSON

   **applications.mediainfo.version:** Optional[string] with version for mediainfo label application

Example:
   arguments = "-f {{item}}"

===================  ================================ ================= =========
Section              Key                              Value             Default
===================  ================================ ================= =========
**Task Definition**  Type                             mediainfo
**Input**
**Output**           mediainfo                        *dict*
**Header**           arguments                        *jinja2 template*
\                    applications.mediainfo.version   *str*             latest
===================  ================================ ================= =========

Melt
####

Execute application Melt (MLT toolkit) with arguments provided.

**Headers:**
   **arguments:** string with Melt arguments parsed as a jinja2 string
                  (workflow variables can be used with jinja2 tags, see https://jinja.palletsprojects.com/en/3.0.x/templates/#variables)

   **applications.melt.version:** Optional[string] with version for melt label application

Example:
   arguments = "{{source}} -consumer {{destination}}"

===================  ===========================   ================= =========
Section              Key                           Value             Default
===================  ===========================   ================= =========
**Task Definition**  Type                          melt
**Input**
**Output**
**Header**           arguments                     *jinja2 template*
\                    applications.melt.version     *str*             latest
===================  ===========================   ================= =========

Python
######

Execute application python with arguments provided

**Headers:**
   **arguments:** string with bash arguments parsed as a jinja2 string
                  (workflow variables can be used with jinja2 tags, see https://jinja.palletsprojects.com/en/3.0.x/templates/#variables)

   **applications.python.version:** Optional[string] with version for python label application

Example:
   arguments = "/path/to/python_script.py {{source}} {{destination}}"

===================  ================================ ================= =========
Section              Key                              Value             Default
===================  ================================ ================= =========
**Task Definition**  Type                             python
**Input**
**Output**
**Header**           arguments                        *jinja2 template*
\                    applications.python.version      *str*             latest
===================  ================================ ================= =========

Scanfolder
##########

Execute a scan of a folder and return a list of files.

**Input Variables:**
   **source:** string with absolute path of the folder to scan

**Output Variables:**
   **items:** list of string of item paths in the folder

**Headers:**
   **recursive:** string ("true" or "false") scan folder recursively if "true"

   **glob_[your suffix]:** Optional[string] glob pattern to filter items. You can have as many glob_suffix as needed

Examples:
   glob_mp4 = "`*`.mp4"

   glob_video = "video_*.*"

===================  ================================ ================= =========
Section              Key                              Value             Default
===================  ================================ ================= =========
**Task Definition**  Type                             scanfolder
**Input**            sources                          *str*
**Output**           items                            *List[str]*
**Header**           recursive                        *bool*
\                    glob_[your_suffix]               *str*             None
===================  ================================ ================= =========

Scenes
######

Get a list of the scenes (shots) of the video from the path provided.

**Input Variables:**
   **source:** string of the source video path

**Output Variables:**
   **scenes:** list of dictionaries with scene informations

.. code-block:: bash

           {
               "index": int,  # int with scene index number
               "start": str,  # Start timecode in second format like 00:00:00:00.000
               "end": str,  # End timecode in second format like 00:00:00:00.000
               "duration": str,  # Duration timecode in second format like 00:00:00:00.000
           }

**Headers:**
   **diff:** Optional[float] of the sensibility level of the detection (default=0.4)

   **applications.ffmpeg.version:** Optional[str] with version for ffmpeg label application

===================  ================================ ================= =========
Section              Key                              Value             Default
===================  ================================ ================= =========
**Task Definition**  Type                             scenes
**Input**            source                           *str*
**Output**           scenes                           *List[dict]*
**Header**           diff                             *float*           0.4
\                    applications.ffmpeg.version      *str*             latest
===================  ================================ ================= =========
