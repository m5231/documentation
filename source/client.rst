Cli client
==========

MADAM is a server. But the python package offers a minimal cli interface for testing purpose.

You can use it to quickly start to create and execute workflows.

Installation
############

First, you need to install ``madam-mam`` python package `from PyPI repository <https://pypi.org/project/madam-mam>`_.

Be sure you have `python 3.8+ <https://www.python.org/downloads/>`_ installed and preferably working in a python virtual environment.

In a python virtual environment, just type:

.. code-block:: bash

    pip install -U madam-mam

Without a python virtual environment, type:

.. code-block:: bash

    pip install --user -U madam-mam

Update
######

To update the package to a new version, simply repeat the same install command.

Usage
#####

If ``madam-mam`` package is correctly installed, you should have a ``madam`` command available:

.. code-block:: bash

    madam

returning the usage help message:

.. code-block:: bash

    Usage: madam [OPTIONS] COMMAND [ARGS]...

      MADAM cli console command

    Options:
      -c, --config PATH  madam.yaml config path  [default: /current/folder/path/madam.yaml]

.. note::

    The configuration file is required only if you run the ``server`` command.
    Go to the :doc:`/installation` chapter to run a server with your own configuration file.

Shell completion
################

Get madam cli bash auto completion with:

.. code-block:: bash

    _MADAM_COMPLETE=source_bash madam > madam-complete.sh
    sudo cp madam-complete.sh /etc/bash_completion.d/.

For another shell, replace `source_bash` by `source_zsh` or `source_fish`

Getting started
###############

You need to specify the domain name or IP address and the port of the server to connect to it.

.. code-block:: bash

    madam --endpoint HOST:PORT [command] ...

For this example, we will use ``localhost:5000``, that you should replace with your own server informations.

Create a workflow file after reading the :doc:`dedicated chapter </workflows>`.

Then type:

.. code-block:: bash

    madam --endpoint localhost:5000 workflow upload PATH/OF/YOUR/WORKFLOW_XML_FILE.bpmn

This command return the unique ID of your workflow version.

Verify that the workflow is in the server:

.. code-block:: bash

    madam --endpoint localhost:5000 workflow list

Execute your workflow by giving the unique ID:

.. code-block:: bash

    madam --endpoint localhost:5000 workflow start WORKFLOW_ID

Show the workflow status and informations:

.. code-block:: bash

    madam --endpoint localhost:5000 workflow show WORKFLOW_ID

If everything went fine, the workflow instance status should be "complete".

Watchfolders
############

BPMN workflows support a Timer in start events. The Cyclic Timer allow to repeatedly trigger the workflow at regular intervals.
This can be used to create a basic watchfolder if the workflow integrate the scanfolder agent.
But it lacks a shared memory between workflow execution to emulate a real watchfolder.

This is why MADAM implements its own watchfolder system.

To trigger your workflows on a folder event, first create a watchfolder.

With the cli client, the easiest way to do it is to create a JSON file with the watchfolder user-defined properties.
Create a file named ``watchfolder.json`` for example, with this template content:

.. code-block:: json

    {
        "path": "/tmp",
        "re_files": ".+.mp4",
        "re_dirs": null,
        "added_workflow": {"id": "WORKFLOW_ID", "version": 1},
        "added_variables": {"my_global_var": "MY_VALUE"},
        "modified_workflow": {"id": "WORKFLOW_ID", "version": 1},
        "modified_variables": {"my_global_var": "MY_VALUE"},
        "deleted_workflow": {"id": "WORKFLOW_ID", "version": 1},
        "deleted_variables":  {"my_global_var": "MY_VALUE"},
        "added_items_key": "my_key_name",
        "modified_items_key": "my_key_name",
        "deleted_items_key": "my_key_name"
    }


Only the ``path`` field is mandatory. All the others are optional.

For each type of event (file added, file modified and file deleted) you can specify a workflow ID and version.
If you omit the ``version`` field or set it to ``null``, the watchfolder will trigger the **latest** version of the workflow.

You can also specified some global variables if you need to with ``*_variables``.

The ``*_items_key`` fields allow you to change the key name of the global variable created by the watchfolder.
By default, the key name is ``watchfolder``. This variable content is the list of the item paths in the folder.

Modify this template to suit your needs. Then send it to the server with this command:

.. code-block:: bash

    madam-cli --endpoint localhost:5000 watchfolder create < watchfolder.json

The workflow ID must be already in the server for this command to be successful.

In response, you'll get the watchfolder ID.

.. code-block:: bash

    Watchfolder ID=603d6462-e4ae-4158-8bd0-0ca617a573e5 created for path /tmp.

Add it in your JSON template file if you need to update the fields later:

.. code-block:: json

    {
        "id": "603d6462-e4ae-4158-8bd0-0ca617a573e5",
        "path": "/tmp",
        "re_files": ".+.mp4",
        "re_dirs": null,
        "added_workflow": {"id": "WORKFLOW_ID", "version": 1},
        "added_variables": {"my_global_var": "MY_VALUE"},
        "modified_workflow": {"id": "WORKFLOW_ID", "version": 1},
        "modified_variables": {"my_global_var": "MY_VALUE"},
        "deleted_workflow": {"id": "WORKFLOW_ID", "version": 1},
        "deleted_variables":  {"my_global_var": "MY_VALUE"},
        "added_items_key": "my_key_name",
        "modified_items_key": "my_key_name",
        "deleted_items_key": "my_key_name"
    }

Then use the ``update`` sub command to update the watchfolder on the server:

.. code-block:: bash

    madam-cli --endpoint localhost:5000 watchfolder update < watchfolder.json

You can now start or stop the watchfolder with ``start``/``stop`` sub command and ``delete`` it.

.. code-block:: bash

    madam-cli --endpoint localhost:5000 watchfolder start WATCHFOLDER_ID

This ends the cli client tutorial.
