.. MADAM documentation master file, created by
   sphinx-quickstart on Tue Apr  7 16:31:46 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. image:: _static/full_bpmn.png

Welcome
=======

This documentation gathers all you need to start using MADAM :superscript:`TM`.

Free Software
#############

MADAM :superscript:`TM` and this documentation are licenced under the terms of `the GPLv3 licence <https://www.gnu.org/licenses/gpl-3.0.en.html>`_.

* Madam source code: https://gitlab.com/m5231/madam
* Documentation source code: https://gitlab.com/m5231/documentation/

MADAM :superscript:`TM` is a registered trademark. The logo was designed by Julie Texier and is also registered.

Support
#######

.. raw:: html

    <script src="https://liberapay.com/vit/widgets/button.js"></script>
    <noscript><a href="https://liberapay.com/vit/donate"><img alt="Donate using Liberapay" src="https://liberapay.com/assets/widgets/donate.svg"></a></noscript>

MADAM :superscript:`TM` is a free (as freedom) software and comes free (as a beer).
If you find it useful for your company and want it to be maintained for a long time, please be kind, and donate.
I will continue to develop and maintain this project as long as the financial support allows me to do so.

Disclaimer
##########

MADAM :superscript:`TM` is proposed **as is** and comes with ABSOLUTELY NO WARRANTY.

Table of Contents
#################

.. toctree::
   :maxdepth: 2

   overview.rst
   installation.rst
   workflows.rst
   client.rst
   distributed.rst
   agents.rst
