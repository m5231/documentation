# MADAM Documentation

Requires python 3.8+

## Python environment

Setup a virtualenv:

    python -m venv .venv

Activate it:

    source .venv/bin/activate

Install python dependencies in virtualenv:

    pip install -r requirements.txt

## Install system requirements (for Ubuntu only, adapt for other distributions)

    ./bin/ubuntu_install_dependencies.sh

## Build with Sphinx

Build the web format:

    make html

Index file location: `build/html/index.html`

Build the pdf format:

    make latexpdf

Pdf file location: `build/latex/madam.pdf`

## Translation

Everytime rst files are modified, you should run this command to update translation po files:

    ./bin/update_i18n.sh

Then edit them to fix or add translations.

Build french version:

    ./bin/build_fr_FR.sh

Build english version:

    ./bin/build_en_US.sh

Build all versions and a main page to all docs:

    ./bin/build.sh

This MADAM documentation is licensed under the [Gnu Public License Version 3](https://www.gnu.org/licenses/gpl-3.0.en.html).
